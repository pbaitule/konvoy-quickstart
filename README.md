# README

# Prerequisites

 - pip
 - docker
 - konvoy
 - awscli
 - awscli credentials
 - konvoy ssh public and private keys copied to the runner in ~/.ssh/

## Updating the cluster

Make changes to the `cluster.yaml` file, like changing the number of worker nodes, and commit with a `[update cluster]` tag in your commit message. This should run the `upgrade_job` of the pipeline.

## Updating the addons

Make changes to the `cluster.yaml` file, like enabling/disabling an addon, and commit with a `[update addons]` tag in your commit message. This should run the `deploy_job` of the pipeline.
