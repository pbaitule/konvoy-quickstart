kind: ClusterProvisioner
apiVersion: konvoy.mesosphere.io/v1beta2
metadata:
  name: konvoy-quickstart
  creationTimestamp: "2020-07-28T22:28:04Z"
spec:
  provider: aws
  aws:
    region: us-east-2
    vpc:
      overrideDefaultRouteTable: true
      enableInternetGateway: true
      enableVPCEndpoints: false
    availabilityZones:
      - us-east-2a
    elb:
      apiServerPort: 6443
    tags:
      owner: pbaitule
  nodePools:
    - name: worker
      count: 3
      machine:
        imageID: ami-01e36b7901e884a10
        rootVolumeSize: 80
        rootVolumeType: gp2
        imagefsVolumeEnabled: true
        imagefsVolumeSize: 160
        imagefsVolumeType: gp2
        imagefsVolumeDevice: xvdb
        type: t3a.medium
    - name: control-plane
      controlPlane: true
      count: 3
      machine:
        imageID: ami-01e36b7901e884a10
        rootVolumeSize: 80
        rootVolumeType: io1
        rootVolumeIOPS: 1000
        imagefsVolumeEnabled: true
        imagefsVolumeSize: 160
        imagefsVolumeType: gp2
        imagefsVolumeDevice: xvdb
        type: t3a.medium
    - name: bastion
      bastion: true
      count: 0
      machine:
        imageID: ami-0affd4508a5d2481b
        rootVolumeSize: 10
        rootVolumeType: gp2
        imagefsVolumeEnabled: false
        type: m5.large
  sshCredentials:
    user: centos
    publicKeyFile: konvoy-quickstart-ssh.pub
    privateKeyFile: konvoy-quickstart-ssh.pem
  version: v1.5.0
---
kind: ClusterConfiguration
apiVersion: konvoy.mesosphere.io/v1beta2
metadata:
  name: konvoy-quickstart
  creationTimestamp: "2020-07-28T22:28:04Z"
spec:
  kubernetes:
    version: 1.17.8
    networking:
      podSubnet: 192.168.0.0/16
      serviceSubnet: 10.0.0.0/18
      iptables:
        addDefaultRules: false
    cloudProvider:
      provider: aws
    admissionPlugins:
      enabled:
        - AlwaysPullImages
        - NodeRestriction
  containerNetworking:
    calico:
      version: v3.13.4
      encapsulation: ipip
      mtu: 1480
  containerRuntime:
    containerd:
      version: 1.3.4
  osPackages:
    enableAdditionalRepositories: true
  nodePools:
    - name: worker
  addons:
    - configRepository: https://github.com/mesosphere/kubernetes-base-addons
      configVersion: stable-1.17-2.0.2
      addonsList:
        - name: awsebscsiprovisioner
          enabled: true
        - name: awsebsprovisioner
          enabled: false
          values: |
            storageclass:
              isDefault: false
        - name: cert-manager
          enabled: true
          values: |
            resources:
              requests:
                cpu: 10m
                memory: 32Mi
        - name: dashboard
          enabled: true
          values: |
            resources:
              requests:
                cpu: 250m
                memory: 300Mi
        - name: defaultstorageclass-protection
          enabled: true
        - name: dex
          enabled: true
          values: |
            resources:
              requests:
                cpu: 100m
                memory: 50Mi
        - name: dex-k8s-authenticator
          enabled: true
          values: |
            resources:
              requests:
                cpu: 100m
                memory: 128Mi
        - name: elasticsearch
          enabled: false
          values: |
            resources:
              requests:
                cpu: 100m
                memory: 1536Mi
        - name: elasticsearch-curator
          enabled: false
          values: |
            resources:
              requests:
                cpu: 100m
                memory: 128Mi
        - name: elasticsearchexporter
          enabled: false
          values: |
            resources:
              requests:
                cpu: 100m
                memory: 128Mi
        - name: external-dns
          enabled: true
          values: |
            aws:
              region:
            domainFilters: []
            resources:
              requests:
                cpu: 10m
                memory: 50Mi
        - name: flagger
          enabled: false
          values: |
            resources:
              requests:
                cpu: 10m
                memory: 32Mi
        - name: fluentbit
          enabled: true
          values: |
            resources:
              requests:
                cpu: 200m
                memory: 200Mi
        - name: gatekeeper
          enabled: true
          values: |
            resources:
              requests:
                cpu: 200m
                memory: 300Mi
        - name: istio # Istio is currently in Preview
          enabled: false
          values: |
            resources:
              requests:
                cpu: 10m
                memory: 50Mi
        - name: kibana
          enabled: false
          values: |
            resources:
              requests:
                cpu: 100m
        - name: konvoyconfig
          enabled: true
        - name: kube-oidc-proxy
          enabled: true
        - name: localvolumeprovisioner
          enabled: false
          values: |
            # Multiple storage classes can be defined here. This allows to, e.g.,
            # distinguish between different disk types.
            # For each entry a storage class '$name' and
            # a host folder '/mnt/$dirName' will be created. Volumes mounted to this
            # folder are made available in the storage class.
            storageclasses:
              - name: localvolumeprovisioner
                dirName: disks
                isDefault: false
                reclaimPolicy: Delete
                volumeBindingMode: WaitForFirstConsumer
        - name: nvidia
          enabled: false
          values: |
            resources:
              requests:
                cpu: 100m
                memory: 128Mi
        - name: opsportal
          enabled: true
          values: |
            landing:
              resources:
                requests:
                  cpu: 100m
                  memory: 128Mi
        - name: prometheus
          enabled: true
          values: |
            prometheus:
              prometheusSpec:
                resources:
                  requests:
                    cpu: 300m
                    memory: 1500Mi
        - name: prometheusadapter
          enabled: true
          values: |
            resources:
              requests:
                cpu: 1000m
                memory: 1000Mi
        - name: reloader
          enabled: true
          values: |
            reloader:
              deployment:
                resources:
                  requests:
                    cpu: 100m
                    memory: 128Mi
        - name: traefik
          enabled: true
          values: |
            resources:
              requests:
                cpu: 500m
        - name: traefik-forward-auth
          enabled: true
          values: |
            resources:
              requests:
                cpu: 100m
                memory: 128Mi
        - name: velero
          enabled: true
          values: |
            resources:
              requests:
                cpu: 250m
                memory: 256Mi
    - configRepository: https://github.com/mesosphere/kubeaddons-conductor
      configVersion: stable-1.17-1.0.0
      addonsList:
        - name: conductor
          enabled: false
    - configRepository: https://github.com/mesosphere/kubeaddons-dispatch
      configVersion: stable-1.17-1.2.2
      addonsList:
        - name: dispatch
          enabled: false
          values: |
            resources:
              requests:
                cpu: 250m
                memory: 256Mi
    - configRepository: https://github.com/mesosphere/kubeaddons-kommander
      configVersion: stable-1.17-1.1.0
      addonsList:
        - name: kommander
          enabled: false
          values: |
            resources:
              requests:
                cpu: 100m
                memory: 256Mi
  version: v1.5.0
