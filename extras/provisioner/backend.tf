terraform {
  backend "s3" {
    bucket = "konvoystatebucket"
    key     = "konvoy"
    region  = "us-east-2"
  }
}
